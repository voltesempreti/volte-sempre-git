#!/usr/bin/env python3

import logging

from PyQt5 import QtCore, QtGui, QtWidgets
from form import Ui_Dialog
from client_git import Client

__version__ = '1.0.2'
__author__  = 'Glemison C Dutra'
__created__ = '03/04/2021'

class Thread(QtCore.QThread):
    logger = QtCore.pyqtSignal(str)
    stop   = QtCore.pyqtSignal(bool)
    def __init__(self, parent, client):
        super(Thread, self).__init__(parent)
        self.client = client
        self.client.logger = self.logger.emit

    def run(self):
        try:
            self.stop.emit(False)
            self.client.init()
            self.client.append_all_files()
            self.client.commit()
            self.client.add_remote()
            self.client.push()
        except Exception as e:
            self.logger.emit(e)
        finally:
            self.stop.emit(True)

class QTextEditLogger(logging.Handler):
    def __init__(self, parent):
        super(QTextEditLogger, self).__init__()
        self.parent = parent
    
    def clear_log(self):
        self.parent.logger_area.clear()

    def emit(self, record):
        msg = self.format(record)
        self.parent.logger_area.appendPlainText(msg)

class App(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.setupUi(self)
        self.logTextBox = QTextEditLogger(self)
        self.logTextBox.setFormatter(logging.Formatter('%(asctime)s- %(message)s', datefmt='%H:%M:%S'))
        logging.getLogger().addHandler(self.logTextBox)
        logging.getLogger().setLevel(logging.DEBUG)

        self.pushButton.setEnabled(False)
        self.pushButton.clicked.connect(self.onClick)
        self.pushButton_2.clicked.connect(self._open_file_dialog)
        self.link.textChanged[str].connect(self.release_button)
        self.is_first_upload = True
        self.show_info()
    
    def show_info(self):
        logging.info('Por: ' + __author__)
        logging.info('Versao: ' + __version__)
        logging.info('Criado em: ' + __created__)
    
    def logger(self, text):
        logging.info(text)
    
    def _open_file_dialog(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory())
        self.directory.setText('{}'.format(directory))
        logging.info('Diretorio carregado com sucesso!')
        logging.info(directory)
        if self.pushButton.isEnabled() is False and self.link.text():
            self.pushButton.setEnabled(True)
    
    def release_button(self, text):
        if len(text) > 10 and self.directory.text():
            self.pushButton.setEnabled(True)
        else:
            self.pushButton.setEnabled(False)

    def releaseK(self):
        self.pushButton.setEnabled(True)
        
    def onClick(self):
        email, username = self.email.text(), self.username.text()
        link, path = self.link.text(), self.directory.text()
        client = Client(path, link, email=email, username=username)
        if not self.is_first_upload:
            self.logTextBox.clear_log()
            self.show_info()
        else:
            self.is_first_upload = False
        if client.is_first_login():
            logging.error('Email e usuario sao obrigatorios no primeiro login!')
        else:
            thread = Thread(self, client)
            thread.logger.connect(self.logger)
            thread.stop.connect(self.pushButton.setEnabled)
            thread.start()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = App()
    ui.show()
    sys.exit(app.exec_())