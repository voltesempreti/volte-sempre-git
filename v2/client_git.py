#!/usr/bin/env python3

import os

def exec_command(command):
    command = 'cmd /c ' + command
    return os.popen(command).readlines()

class Client:
    def __init__(self, path_dir, link, rname='origin', email=None, username=None, commit='commit'):
        self.path_dir = path_dir
        self.email    = email
        self.username = username
        self.rname    = rname
        self.rlink    = link
        self._commit   = commit
        self.logger   = None or print
        os.chdir(path_dir)
    
    def __remove_folder_git(self):
        if os.path.isdir('.git'):
            self.logger('Removendo diretorio .git...')
            exec_command('RMDIR /s /q .git')
    
    def __set_config_login(self):
        cmd = 'git config --global user.email %s'
        cmd += '\ngit config --global user.name %s'
        return os.system(cmd % (self.email, self.username))
    
    def is_first_login(self):
        data = exec_command('git config --get-all user.email')
        if not data:
            return True
        return False
    
    def init(self):
        if self.email and self.username:
            self.__set_config_login()
            self.logger('E-mail e usuario foram aplicados! %s:%s' % (self.email, self.username))
        self.__remove_folder_git()
        exec_command('git init')
        self.logger('Repositório Git vazio inicializado em: ' + self.path_dir)
        return True
    
    def append_all_files(self):
        exec_command('git add -A')

    def commit(self):
        exec_command('git commit -m ' + self._commit)
        self.logger('Commit aplicado com sucesso!')
    
    def add_remote(self):
        exec_command('git remote add %s %s' % (self.rname, self.rlink))
    
    def push(self):
        self.logger('Fazendo push...')
        exec_command('git push --force -u %s master' % self.rname)
        self.logger('Push finalizado com sucessso!')